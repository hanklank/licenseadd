        include lvo/exec_lib.i
        include lvo/dos_lib.i
 
        ; open DOS library
        movea.l  4.w,a6
        lea      dosname(pc),a1
        moveq    #36,d0
        jsr      _LVOOpenLibrary(a6)
        movea.l  d0,a6
 
        ; actual print string
        lea      hellostr(pc),a0
        move.l   a0,d1
        jsr      _LVOPutStr(a6)
 
        ; close DOS library
        movea.l  a6,a1
        movea.l  4.w,a6
        jsr      _LVOCloseLibrary(a6)
        rts
 
dosname     dc.b 'dos.library',0
hellostr    dc.b 'Hello, world!',0
