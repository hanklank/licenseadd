import tempfile
import unittest
from shutil import copyfile

from collections import defaultdict
from license_add import LicenseAdd
from license import License
from language import Language

class TestBasicFunction(unittest.TestCase):
    
    template_list = []
    tempdir = tempfile.mkdtemp()

    @classmethod
    def setUpClass(cls):
        cls.setup_licensedictionary()

    def setUp(self):
        self.func = LicenseAdd()
        print(self.template_list)

    def test_add_license_c_gplv3(self):

        self.add_license(Language.C.value,License.GPLv3.value)

    def test_add_license_c_lgplv3(self):
        
        self.add_license(Language.C.value,License.LGPLv3.value)
    
    def test_add_license_java_gplv3(self):
        
        self.add_license(Language.JAVA.value,License.GPLv3.value)
    
    def test_add_license_java_lgplv3(self):
        
        self.add_license(Language.JAVA.value,License.LGPLv3.value)
    
    def add_license(self, lang, license):

        copyfile(self.get_sourcefile(lang), self.get_tmpsourcecopy(lang)) 
       
        no_license = self.file_to_string(self.get_tmpsourcecopy(lang))
        with_license = self.file_to_string(self.get_sourcefile_with_license(lang,license))

        self.assertNotEqual(no_license, with_license)

        self.func.process([f'-wd={self.tempdir}',f'-od={self.tempdir}','-y=1973', '-co=COPYLEFT','-pn=SUPERPROGRAM', f'-lng={lang}', f'-l={license}'])

        no_license = self.file_to_string(self.get_tmpsourcecopy(lang))
        self.assertEqual(no_license, with_license)

    def get_sourcefile(self, lang):
        return f'test/resources/{lang}/hello.{lang}'

    def get_sourcefile_with_license(self, lang, license):
        return f'test/resources/{lang}/{license}/hello_{license}.{lang}'
    
    def get_tmpsourcecopy(self, lang):
        return self.tempdir + f'/hello.{lang}'

#def test_add_license_c_gpl3_insert_at_first_empty(self):
#       self.func.process(['1973', 'COPYLEFT','SUPERPROGRAM', 'c', 'gplv3','-ex=hello_'])

#   
#   def test_add_license_c_lgpl_insert_at_first_empty(self):
#       self.func.process(['1973', 'COPYLEFT','SUPERPROGRAM', 'c', 'lgplv3','-ex=hello_'])

#   def test_add_license_c_gpl3_no_special_insert(self):
#       #year, copyright_owner, program, language, license
#       parser = self.func.parse_arguments(['1973', 'COPYLEFT','SUPERPROGRAM', 'COBOL', 'SUPERLICENSE'])

    @classmethod 
    def setup_licensedictionary(self):
        dic = defaultdict(list)

        dic['gplv3'].append('c')
        dic['gplv3'].append('py')
        dic['gplv3'].append('68k')
       
        dic['lgplv3'].append('c')
        dic['lgplv3'].append('py')
        dic['lgplv3'].append('68k')
        
        return self.templates_to_dictionary(dic)

    @classmethod
    def templates_to_dictionary(self, dic):
        for key, value in dic.items():
            for x in value:
                self.template_list.append(key + '_' + x + '_' + 'headertemplate.txt')
        
        #with open('data.txt', 'r') as myfile:
         #   data = myfile.read()

    def file_to_string(self, path):
        with open(path, 'r') as f:
            return f.read().replace('\n', '')

if __name__ == '__main__':
    unittest.main()
