import sys
import logging
import glob

class LicenseReader: 

    def __init__(self):
        self.logger = logging.getLogger('licenseadd_logger')

    def read_licensefile(self, args):
        try:  
            licensefile  = None
            print(args)
            
            with open( 'templates/' + args.license + "_" + args.language + "_headertemplate.txt", "r", encoding="utf8") as f:

                licensefile = f.read()
                licensefile = licensefile.replace('{PROGRAM}', args.programname)
                licensefile = licensefile.replace('{AUTHOR}', args.copyright_owner)
                licensefile = licensefile.replace('{YEAR}', args.year)

                #self.logger.info(licensefile)
        except FileNotFoundError as fe:
            self.logger.error(f"Filepath not found: {fe}")
            sys.exit()

        return licensefile


        
    def read_filenames(self, args):
        list = []
        language = args.language
        for pattern in self.globpattern(language, args.workdir):
                filepath = glob.iglob(pattern, recursive=True)
                list.extend(filepath)


        return self.exclude_file(args, list)
    
    def globpattern(self, language, workdir):
        fileexts = None

        if language == "c":
            fileexts=['**/*.c','**/*.h', '**/*.C', '**/*.H']
        elif language == "68k":
            fileexts=['**/*.a', '**/*.asm', '**/*.i', '**/*.A', '**/*.ASM', '**/*.I', '**/*.s', '**/*.S']
        elif language == 'py':
            fileexts=['**/*.py', '**/*.PY']
        elif language == 'java':
            fileexts=['**/*.java']

       
        patterns = []

        for extension in fileexts:
            patterns.append(workdir + '/' + extension)
            self.logger.info(workdir + '/' + extension)

        print(patterns)
        print(fileexts)
        return patterns

    def exclude_file(self, args, filenames):
        if args.exclude:        
            for exclude in args.exclude:
                filenames = [path for path in filesnames if exclude not in path]
        return filenames

