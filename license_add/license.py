from enum import Enum

class License(Enum):
    GPLv3='gplv3'
    LGPLv3='lgplv3'

