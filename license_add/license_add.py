#!/usr/bin/env python3

import logging
import sys

import licensewriter
import argumentparse
import licensereader

class LicenseAdd(object):

    def __init__(self):
        self.setup_log()

    def process(self, args):
        
        argparser = argumentparse.ArgumentParse()
        argparser.is_args_empty()
        args = argparser.parse(args)
        print(args.workdir)
        print(args.outputdir)

        reader = licensereader.LicenseReader()
        license = reader.read_licensefile(args)
        filenames = reader.read_filenames(args)

        writer = licensewriter.LicenseWriter()
        writer.add_license(args, filenames, license, args.firstempty)


    def setup_log(self):

        self.logger = logging.getLogger('licenseadd_logger')
        self.logger.setLevel(logging.DEBUG)
       
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)

        self.logger.addHandler(ch)


if __name__== "__main__":
    licenseAdd = LicenseAdd()
    licenseAdd.process(sys.argv[1:])
