from pathlib import Path
import time
import argparse
import tempfile
import sys

from license import License
from language import Language

class ArgumentParse(): 

    def __init__(self):
        
        tmpoutputdir = tempfile.mkdtemp() 

        self.parser = argparse.ArgumentParser(description='LicenseAdd - Add a license header to files.')
        self.parser.add_argument('-ex','--exclude', nargs='?', const="", action='append')
        self.parser.add_argument('-fe','--firstempty', help='Insert at first empty line',
                action="store_true")
        self.parser.add_argument('-wd','--workdir', nargs='?', type=self.dir_exists, const="WORKDIR", default="WORKDIR")
        self.parser.add_argument('-od','--outputdir', nargs='?',type=self.dir_exists, const=tmpoutputdir, default=tmpoutputdir)
        required_named = self.parser.add_argument_group('required named arguments')
        required_named.add_argument('-y','--year', help='The copyright year', required=True, type=self.is_valid_year)
        required_named.add_argument('-co','--copyright_owner', help='The copyright owner', required=True)
        required_named.add_argument('-pn','--programname', help='The program name', required=True)
        required_named.add_argument('-lng','--language', help='Values: c, java, py or 68k', required=True,type=self.is_supported_language)
        required_named.add_argument('-l','--license', help='Values: lgplv3 or gplv3', required=True,type=self.is_supported_license)

        self.is_args_empty()

    def is_args_empty(self):
        if len(sys.argv) <= 1:
            print('No arguments. Please see -h for help')
            sys.exit()
        return False

    def dir_exists(self, path):
        p = Path(path)
        if not p.exists() or not p.is_dir():
            raise argparse.ArgumentTypeError(f'{path} is not a directory, please give an valid argument')
        else:
            return path

    def is_valid_year(self, year):
        try: 
            valid_year = time.strptime(year, '%Y')
            return year
        except ValueError:
            raise argparse.ArgumentTypeError(f'{year} is not a valid year')

    def is_supported_license(self, license):
        for l in License:
            if license == l.value:
               return license
        raise argparse.ArgumentTypeError(f'{license} is not a supported license')
    
    def is_supported_language(self, language):
        for l in Language:
            if language == l.value:
               return language
        raise argparse.ArgumentTypeError(f'{language} is not a supported license')

    def parse(self, args):
        return self.parser.parse_args(args)
