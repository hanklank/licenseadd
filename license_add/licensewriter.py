import logging
import os
import re

class LicenseWriter:

    def __init__(self):
        self.logger = logging.getLogger('licenseadd_logger')


    def add_license(self, args, filenames, license, firstfree):
        print(filenames)

        for filename in filenames:
            self.logger.debug('Working with file: ' + filename)

            filee = self.read_codefile(filename)

            output = self.insert_license(filee,license, firstfree)

            if args.outputdir:
                filename = args.outputdir + self.trim_workdir(filename,args.workdir)
    
            self.logger.debug('Working with file2: ' + filename)
        
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            
            with open(filename, "w") as text_file:
                print(output, file=text_file)

    def trim_workdir(self, f, workdir):

        print(workdir)
        print(f)
        print(f.replace(workdir,''))
        return f.replace(workdir,'')

    def insert_license(self, filee, license, firstfree):
        index = self.get_insert_index(firstfree, filee)
        return filee[:index] + license + filee[index:]

    def get_insert_index(self, find_empty_line, filee):
        index = 0

        if find_empty_line:
            match = re.search("\n\s*?\n",filee)
            if match is not None:
                index = match.start()
        return index


    def read_codefile(self, filename):
        codefile = None

        with open( filename, "r+", encoding="utf8") as f:
            codefile = f.read()

        return codefile

