from enum import Enum

class Language(Enum):
    JAVA='java'
    C='c'
    PYTHON='python'
    ASM68K='68k'
